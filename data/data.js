var json = [{
  id: 1,
  name: '炒菜',
  items: [{
    id: 1,
    title: '青椒小炒肉',
    price: 6,
    active: false,
    num: 1,
    image: '/images/item-m.jpg'
  }, {
    id: 2,
    title: '西红柿炒蛋',
    price: 5,
    active: false,
    num: 1,
    image: '/images/fanqie.jpg'
  }, {
    id: 3,
    title: '青椒炒蛋',
    price: 5,
    active: false,
    num: 1,
    image: '/images/qjcd.jpg'
  }, {
    id: 4,
    title: '白灼虾',
    price: 15,
    active: false,
    num: 1,
    image: '/images/bzx.jpg'
  }, {
    id: 5,
    title: '可乐鸡翅',
    price: 8,
    active: false,
    num: 1,
    image: '/images/jc.jpg'
  }, {
    id: 20,
    title: '土豆丝',
    price: 4,
    active: false,
    num: 1,
    image: '/images/tds.jpg'
  }, {
    id: 21,
    title: '南瓜蒸排骨',
    price: 8,
    active: false,
    num: 1,
    image: '/images/ngzpg.jpg'
  }, {
    id: 22,
    title: '淮山木耳荷兰豆',
    price: 8,
    active: false,
    num: 1,
    image: '/images/ysmr.jpg'
  }, {
    id: 23,
    title: '宫保鸡丁',
    price: 8,
    active: false,
    num: 1,
    image: '/images/gbjd.jpg'
  }, {
    id: 24,
    title: '芹菜炒牛肉',
    price: 8,
    active: false,
    num: 1,
    image: '/images/qccn.jpg'
  }]
}, {
  id: 2,
  name: '煲汤',
  items: [{
    id: 6,
    title: '淮山煲鸡',
    price: 12,
    active: false,
    num: 1,
    image: '/images/hsbj.jpg'
  }, {
    id: 7,
    title: '玉米胡萝卜排骨汤',
    price: 12,
    active: false,
    num: 1,
    image: '/images/fxympg.jpg',
  }, {
    id: 8,
    title: '西红柿鸡蛋汤',
    price: 9,
    active: false,
    num: 1,
    image: '/images/xhsjd.jpg',
  }, {
    id: 9,
    title: '莲藕排骨汤',
    price: 9,
    active: false,
    num: 1,
    image: '/images/gtlo.jpg',
  }, {
    id: 10,
    title: '猪肚鸡汤',
    price: 9,
    active: false,
    num: 1,
    image: '/images/zdj.jpg',
  }, {
    id: 11,
    title: '鲫鱼豆腐汤',
    price: 9,
    active: false,
    num: 1,
    image: '/images/jydf.jpg',
  }]
}, {
  id: 3,
  name: '青菜',
  items: [{
      id: 15,
      title: '生菜',
      price: 6,
      active: false,
      num: 1,
      image: '/images/shengcai.jpg',
    },
    {
      id: 16,
      title: '菜心',
      price: 6,
      active: false,
      num: 1,
      image: '/images/cx.jpg',
    },
    {
      id: 17,
      title: '空心菜',
      price: 6,
      active: false,
      num: 1,
      image: '/images/kxc.jpg',
    },
    {
      id: 18,
      title: '油麦菜',
      price: 6,
      active: false,
      num: 1,
      image: '/images/ymc.jpg',
    },
    {
      id: 19,
      title: '茼蒿',
      price: 6,
      active: false,
      num: 1,
      image: '/images/th.jpg',
    }
  ]
}]
module.exports = {
  list: json
}